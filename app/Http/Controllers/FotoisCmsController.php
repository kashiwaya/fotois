<?php

namespace App\Http\Controllers;

use App\Libs\imgedit;
use Illuminate\Http\Request;
use App\Image;  //←Modelをuseしてメソッドの記述を簡潔に。

class FotoisCmsController extends Controller
{
    //ログイン画面
    public function loginGet(){
        return view('cms.login');
    }
    //ログイン後、一覧画面に遷移
    public function loginPost(){
        return redirect('/manage/list');
    }

    //一覧画面(get)
    public function list(){
        //$images = Image::all();
        $images = Image::orderBy('updated_at','desc')->get();
        return view('cms.list')->with('image',$images);
    }

    //新規登録画面
    public function upload(){
        return view('cms.upload');
    }
    //保存
    public function saveimg(Request $img){

        //モデルをインスタンス化
        $image = new Image();

        //代入
        $image->title = $img->title;                                                //タイトル
        $image->caption = $img->caption;                                            //キャプション

        /*
        \Request::file("")でもいけるけど、
        public function saveimg(Request $img)　の引数 $imgがRequestクラスなので、
        ファイルを操作したい場合は$img->file("file")でも操作できたりします。
        そっちの方が記述量が少なくて楽かもしれないです。
                                 ↓
        */
        $image->extension = $img->file("file")->getClientOriginalExtension();

        //重複しないようにファイル名作成
        $originalFileName = str_random(10) . "_" . $img->file("file")->getClientOriginalName();
        $image->filename = $originalFileName;
        $imgpath = public_path()."/img/";
        $image->savepath = $imgpath;

        //INSERT
        $image->save();


        //サムネイル作成
        $imageEdit = new imgedit();     //画像処理クラスをインスタンス化
        //リサイズメソッド呼び出し
        $imageEdit->resize($img->file("file"),150,null,$originalFileName,$imgpath."thumb");

        //ファイル移動（本体）
        $img->file("file")->move($imgpath,$originalFileName);
        return redirect('/manage/list')->with('flash_message','登録完了！');

    }

    //削除
    public function destroy($id){
        //DBから削除
        $image = Image::find($id);
        $image->delete();
        //ファイル削除

        return redirect('/manage/list')->with('flash_message',$id.'_'.'削除完了！');
    }

    //検索結果
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchresult(Request $request){
        $keyword = $request->input('keyword');
        return view('cms.searchresult',compact('keyword'));
    }

}
