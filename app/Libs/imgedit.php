<?php

namespace App\Libs;

use Image;
use Intervention\Image\Constraint;

class imgedit
{
    /**
     * @param $img
     * @param $w
     * @param null $h
     * @param $savename
     * @param $savepath
     * @return \Intervention\Image\Image
     */
    public function resize($img, $w, $h = null, $savename, $savepath){
/*
        Image::make($img)
            ->resize($w,$h,function($constraint) {
                $constraint->aspectRatio();
            })
            ->save($savepath."/s_".$savename);
*/
        Image::make($img)
            ->resize($w,$h,function(Constraint $constraint) {
                $constraint->aspectRatio();
            })
            ->save($savepath."/s_".$savename);

    }
}