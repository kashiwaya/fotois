<style>
    .boxContainer{
        overflow: hidden;
    }

    .box{
        border: solid 1px;
        margin: 5px;
        padding: 5px;
        float: left;
    }

    .title{
        border-top: dashed 1px;
    }

    /* clearfix */
    .boxContainer:before,
    .boxContainer:after {
        content: "";
        display: table;
    }

    .boxContainer:after {
        clear: both;
    }

    /* For IE 6/7 (trigger hasLayout) */
    .boxContainer {
        zoom: 1;
    }

    .button {
        display: inline-block;
        width: 200px;
        height: 54px;
        text-align: center;
        text-decoration: none;
        line-height: 54px;
        outline: none;
        background-color: #333;
        color: #fff;
    }
    .button::before,
    .button::after {
        position: absolute;
        z-index: -1;
        display: block;
        content: '';
    }
    .button,
    .button::before,
    .button::after {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-transition: all .3s;
        transition: all .3s;
    }
    .button:hover {
        background-color: #59b1eb;
    }

    .message{
        font-style: italic;
        font-weight: bold;
    }

</style>
<div class="container">
    <h1>CMS一覧画面</h1>
    @if(Session::get('flash_message'))
        <div class="message">{{ session('flash_message') }}</div>
    @endif
    <form action="/manage/searchresult" method="post">
        {{ csrf_field() }}
        <p>検索キーワード:<input type="text" name="keyword"><input type="submit" value="検索"></p>
    </form>

    <div>
        <a href="/manage/upload" class="button">画像アップロード</a>
    </div>

    <div class="boxContainer">
        <p>ここに画像リスト（サムネイル）を表示して、件数に応じてページングする。</p>
        @foreach($image as $img)
            <div class="box">
                <div><img src="/img/thumb/s_{{ $img->filename }}"></div>
                <p class="title">{{ $img->title }}</p>
                <p>{{ $img->caption }}</p>
                <form action="/manage/del/{{ $img->id }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}
                    <input type="submit" value="削除">
                </form>
            </div>
            
        @endforeach
    </div>

</div>

