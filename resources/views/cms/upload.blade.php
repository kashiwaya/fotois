<div class="container">
    <h1>CMS画像登録画面</h1>
    <form action="/manage/saveimg" method="post" enctype='multipart/form-data'>
        {{ csrf_field() }}
        <p>ファイル:</p>
        <p><input type="file" name="file"></p>
        <p>タイトル:</p>
        <p><input type="text" name="title"></p>
        <p>キャプション:</p>
        <p><textarea name="caption"></textarea></p>
        <input type="submit" value="登録">
    </form>

</div>
