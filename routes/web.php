<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function(){
    //FRONT
    Route::get('/','FotoisController@index');                           //TOPページ(get)

    //CMS
    Route::get('/manage','FotoisCmsController@loginGet');                           //CMS：ログイン(get)
    Route::post('/manage','FotoisCmsController@loginPost');                         //CMS：ログイン(post)
    Route::get('/manage/list','FotoisCmsController@list');                          //CMS：一覧
    Route::post('/manage/list/{keyword}','FotoisCmsController@listSearch');         //CMS：一覧(検索)
    Route::delete('/manage/del/{id}','FotoisCmsController@destroy');            	//CMS：削除
    Route::get('/manage/detail/{id}','FotoisCmsController@detail');                 //CMS：詳細
    Route::post('/manage/searchresult','FotoisCmsController@searchresult');         //CMS：検索結果
    Route::get('/manage/upload','FotoisCmsController@upload');                      //CMS：新規登録画面
    Route::post('/manage/saveimg','FotoisCmsController@saveimg');                   //CMS：新規登録(save)
    Route::get('/manage/edit/{id}','FotoisCmsController@edit');                     //CMS：編集画面
    Route::patch('/manage/edit/{id}','FotoisCmsController@update');                 //CMS：編集
});
